# Cake Manager Micro Service (fictitious)

## Instructions
Please have `docker-compose` and `mvn` on your path. Run `docker-compose up`. Once the docker containers are running, you can access:
 
- the UI at http://localhost:8080/ 
- The API at http://localhost:8081
- Prometheus at http://localhost:9090/graph
  - As an example, timing for `cakeServer_getAllCakes` is collected everytime the `/cakes` API end point is hit, as well as various other system metrics
- cAdvisor at http://localhost:8083
    - http://localhost:8083/docker/cadvisor
    - http://localhost:8083/docker/prometheus
    - http://localhost:8083/docker/cakemgr-client
    - http://localhost:8083/docker/cakemgr-server


## Areas for improvement (non-exhaustive list)

- Make sure `image` property is a valid URL
- Ensure inputs are safe (escape any HTML)
- Validation on UI
- Error handling on UI
- OAuth2 (potentially uing Spring Security)

## Notes
- The given initial JSON URL contains duplicates, but the original project had a requirement for `title` to be unique. Therefore, data integrity violation errors will appear in the log at start up.