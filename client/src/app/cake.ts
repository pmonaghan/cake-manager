export interface Cake {
  id: number;
  title: string;
  desc: string;
  image: string;
}
