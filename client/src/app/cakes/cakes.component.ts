import { Component, OnInit } from '@angular/core';
import { Cake } from '../cake'
import { CakesService } from './cakes.service';

@Component({
  selector: 'app-cake-manager',
  templateUrl: './cakes.component.html',
  styleUrls: ['./cakes.component.scss']
})
export class CakesComponent implements OnInit {

  title = 'Cake Manager Micro Service (fictitious)';
  cakes: Cake[];
  constructor(private cakesService: CakesService) {}

  ngOnInit() {
      this.getCakes();
   }

  getCakes(): void {
    this.cakesService.getCakes()
        .subscribe(cakes => this.cakes = cakes);
  }

  addCake(title : string, desc : string, image : string): void {
    let id = null;
    this.cakesService.addCake({ id, title, desc, image } as Cake)
        .subscribe(cake => {
          this.cakes.push(cake);
        });
  }

}
