import { Injectable } from '@angular/core';
import { Cake } from '../cake';
import { CAKES } from '../mock-cakes'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CakesService {

  private cakesUrl = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getCakes(): Observable<Cake[]> {
       return this.http.get<Cake[]>(this.cakesUrl)
  }

  addCake(cake: Cake): Observable<Cake> {
    return this.http.post<Cake>(this.cakesUrl, cake, this.httpOptions).pipe(
      tap((newCake: Cake) => console.log(`added cake w/ id=${newCake.id}`)),
      catchError(this.handleError<Cake>('addHero'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
        console.error(error);
        return of(result as T);
    };
  }
}
