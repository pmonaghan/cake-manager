package com.waracle.cakemgr.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.dao.CakeService;
import com.waracle.cakemgr.model.Cake;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static com.waracle.cakemgr.model.builder.CakeBuilder.aCake;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class CakeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CakeController cakeController;

    @MockBean
    private CakeService cakeService;

    @Test
    public void GIVEN_a_call_to_get_cakes_THEN_return_cake_list() throws Exception {
        Cake firstCake = aCake()
                .withId(1L)
                .withTitle("Genoa cake")
                .withDesc("A cake Mary Berry would be proud of!")
                .withImage("https://gph.is/2d9ucXz")
                .build();

        Cake secondCake = aCake()
                .withId(2L)
                .withTitle("Linzer torte")
                .withDesc("Austrian pastry")
                .withImage("http://gph.is/2uC5jfK")
                .build();

        Mockito.when(cakeService.getAllCakes()).thenReturn(Arrays.asList(firstCake, secondCake));

        mockMvc.perform(get("/cakes"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].title", is(firstCake.getTitle())))
                .andExpect(jsonPath("$[0].desc", is(firstCake.getDesc())))
                .andExpect(jsonPath("$[0].image", is(firstCake.getImage())))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].title", is(secondCake.getTitle())))
                .andExpect(jsonPath("$[1].desc", is(secondCake.getDesc())))
                .andExpect(jsonPath("$[1].image", is(secondCake.getImage())));
    }

    @Test
    public void GIVEN_valid_input_THEN_save_cake() throws Exception {
        Cake cake = new Cake();
        cake.setTitle("Caterpillar cake");
        cake.setDesc("Colin the Caterpillar");
        cake.setImage("https://gph.is/g/ZynnyAO");

        mockMvc.perform(post("/cakes")
                .content(writeCakeAsJson(cake))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void GIVEN_blank_fields_that_are_required_THEN_reject_input() throws Exception {
        Cake cake = new Cake();

        String descError = "Input for field 'desc' is invalid: Description is required.";
        String titleError = "Input for field 'title' is invalid: Title is required.";
        String imageError = "Input for field 'image' is invalid: Image is required.";

        mockMvc.perform(post("/cakes")
                .content(writeCakeAsJson(cake))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.message", is("Invalid input")))
                .andExpect(jsonPath("$.errors", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.errors", Matchers.containsInAnyOrder(descError, titleError, imageError)));
    }

    @Test
    public void GIVEN_input_fields_expect_max_size_THEN_reject_input() throws Exception {
        Cake cake = new Cake();
        cake.setDesc(RandomStringUtils.randomAlphabetic(101));
        cake.setTitle(RandomStringUtils.randomAlphabetic(101));
        cake.setImage(RandomStringUtils.randomAlphabetic(301));

        String descError = "Input for field 'desc' is invalid: size must be between 1 and 100";
        String titleError = "Input for field 'title' is invalid: size must be between 1 and 100";
        String imageError = "Input for field 'image' is invalid: size must be between 1 and 300";

        mockMvc.perform(post("/cakes")
                .content(writeCakeAsJson(cake))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")))
                .andExpect(jsonPath("$.message", is("Invalid input")))
                .andExpect(jsonPath("$.errors", Matchers.hasSize(3)))
                .andExpect(jsonPath("$.errors", Matchers.containsInAnyOrder(descError, titleError, imageError)));
    }

    @Test
    public void GIVEN_invalid_input_THEN_reject() throws Exception {
        String invalidData = "{'notARealField': 'this is a test'}";
        mockMvc.perform(post("/cakes")
                .content(invalidData)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    private String writeCakeAsJson(Cake cake) throws JsonProcessingException {
        return new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .writeValueAsString(cake);
    }

}