package com.waracle.cakemgr.dao;

import com.waracle.cakemgr.model.Cake;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CakeService {

    private static final Logger LOG = LoggerFactory.getLogger(CakeService.class);

    private final CakeRepository cakeRepository;
    private final CompositeMeterRegistry meterRegistry;

    @Autowired
    public CakeService(CakeRepository cakeRepository, CompositeMeterRegistry meterRegistry) {
        this.cakeRepository = cakeRepository;
        this.meterRegistry = meterRegistry;
    }

    public Cake saveCake(Cake cake) {
        return cakeRepository.save(cake);
    }

    public List<Cake> getAllCakes() {
        try{
            Timer timer = Timer.builder("cakeServer.getAllCakes")
                    .publishPercentileHistogram()
                    .register(meterRegistry);

            return timer.record(() -> getAllCakesFromDb());
        } catch (Exception e) {
            LOG.warn("Failed to record time to get all cakes from DB", e);
            return getAllCakesFromDb();
        }
    }

    private List<Cake> getAllCakesFromDb() {
        ArrayList<Cake> cakes = new ArrayList<>();
        cakeRepository.findAll().forEach(cake -> cakes.add(cake));
        return cakes;
    }
}
