package com.waracle.cakemgr.model.builder;

import com.waracle.cakemgr.model.Cake;

public final class CakeBuilder {
    private Long id;
    private String title;
    private String desc;
    private String image;

    private CakeBuilder() {
    }

    public static CakeBuilder aCake() {
        return new CakeBuilder();
    }

    public CakeBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public CakeBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public CakeBuilder withDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public CakeBuilder withImage(String image) {
        this.image = image;
        return this;
    }

    public Cake build() {
        Cake cake = new Cake();
        cake.setId(id);
        cake.setTitle(title);
        cake.setDesc(desc);
        cake.setImage(image);
        return cake;
    }
}
