package com.waracle.cakemgr.model;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Cake {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title", unique = true, nullable = false, length = 100)
    @NotBlank(message = "Title is required.")
    @Size(min = 1, max = 100)
    private String title;

    @Column(name = "description", unique = false, nullable = false, length = 100)
    @NotBlank(message = "Description is required.")
    @Size(min = 1, max = 100)
    private String desc;

    @Column(name = "image", unique = false, nullable = false, length = 300)
    @NotBlank(message = "Image is required.")
    @Size(min = 1, max = 300)
    private String image;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
