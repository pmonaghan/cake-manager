package com.waracle.cakemgr;

import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.waracle.cakemgr")
public class CakemgrApplication {

	public static void main(String[] args) {
		SpringApplication.run(CakemgrApplication.class, args);
	}

	@Bean
	public CompositeMeterRegistry meterRegistry() {
		CompositeMeterRegistry compositeMeterRegistry = new CompositeMeterRegistry();
		compositeMeterRegistry.add(new PrometheusMeterRegistry(PrometheusConfig.DEFAULT));

		compositeMeterRegistry.config().commonTags("server", "cakemgr-server");

		new ProcessorMetrics().bindTo(compositeMeterRegistry);
		new JvmMemoryMetrics().bindTo(compositeMeterRegistry);
		new JvmGcMetrics().bindTo(compositeMeterRegistry);
		new JvmThreadMetrics().bindTo(compositeMeterRegistry);
		new ClassLoaderMetrics().bindTo(compositeMeterRegistry);
		new UptimeMetrics().bindTo(compositeMeterRegistry);

		return compositeMeterRegistry;
	}
}
