package com.waracle.cakemgr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.dao.CakeService;
import com.waracle.cakemgr.model.Cake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;

@Component
public class CakeMgrStartupRunner implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(CakeMgrStartupRunner.class);
    private final CakeService cakeService;

    @Value("${cake.json.url}")
    private String cakeURLString;

    @Autowired
    public CakeMgrStartupRunner(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Attempting to retrieve cake json");
        try {
            ObjectMapper mapper = new ObjectMapper();
            URL cakeURL = new URL(cakeURLString);
            List<Cake> cakes = mapper.readValue(cakeURL, new TypeReference<List<Cake>>() {});
            saveCakes(cakes);
        } catch (Exception e) {
            LOG.warn("Unable to load initial cakes", e);
        }
    }

    private void saveCakes(List<Cake> cakes) {
        cakes.forEach(cake -> {
            try {
                cakeService.saveCake(cake);
            } catch (DataIntegrityViolationException e) {
                LOG.error("Unable to save cake due to data integrity violation. Does this title already exist?");
            } catch (Exception e) {
                LOG.error("Unable to save cake", e);
            }
        });
    }
}
