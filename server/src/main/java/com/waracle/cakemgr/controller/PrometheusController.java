package com.waracle.cakemgr.controller;

import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RestController
@CrossOrigin(maxAge = 3600)
public class PrometheusController {

    @Autowired
    private CompositeMeterRegistry meterRegistry;

    @GetMapping("/prometheus")
    public void scrape(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        Optional<PrometheusMeterRegistry> meterReg = meterRegistry.getRegistries()
                .stream()
                .filter(PrometheusMeterRegistry.class::isInstance)
                .map(PrometheusMeterRegistry.class::cast)
                .findFirst();
        if(meterReg.isPresent()) {
            String meterRegData = meterReg.get().scrape();
            resp.getOutputStream().write(meterRegData.getBytes());
        } else {
            throw new RuntimeException("Meter registry not present");
        }
    }
}
