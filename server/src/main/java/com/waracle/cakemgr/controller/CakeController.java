package com.waracle.cakemgr.controller;

import com.waracle.cakemgr.dao.CakeService;
import com.waracle.cakemgr.model.Cake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@CrossOrigin(maxAge = 3600)
public class CakeController {

    private final CakeService cakeService;

    @Autowired
    public CakeController(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    @GetMapping("/cakes")
    public List<Cake> index() {
        return cakeService.getAllCakes();
    }

    @PostMapping("/cakes")
    public Cake newCake(@Valid @RequestBody Cake cake) {
        return cakeService.saveCake(cake);
    }

}
