package com.waracle.cakemgr.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;

@ControllerAdvice
public class CakeMgrApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ArrayList<String> validationErrors = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((err) -> {
            String fieldName = ((FieldError) err).getField();
            String errorMessage = String.format("Input for field '%s' is invalid: %s", fieldName, err.getDefaultMessage());
            validationErrors.add(errorMessage);
        });

        CakeMgrApiError error = new CakeMgrApiError(HttpStatus.BAD_REQUEST, "Invalid input", validationErrors);
        return handleExceptionInternal(ex, error, headers, error.getStatus(), request);
    }
}
